#pragma once

int pts_init(void);
void pts_cleanup(void);

int pts_hot_pages(size_t sz, size_t * addrs);
