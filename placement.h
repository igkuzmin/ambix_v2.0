#pragma once

int placement_init(void);
void placement_cleanup(void);

int placement_check_memory(void);

int placement_bind_pid(pid_t pid);
int placement_unbind_pid(pid_t pid);

//int placement_mem_walk(int n, int mode);
//int placement_clear_walk(int mode);
//int placement_switch_walk(int n);

//int placement_find(int n_pages, int mode);
