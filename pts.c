//#define DEBUG
#define pr_fmt(fmt) "pts_kmod.PTS: " fmt


#include <linux/hashtable.h>
#include <linux/kernel.h>  // Contains types, macros, functions for the kernel
#include <linux/pagewalk.h>
#include <linux/slab.h>
#include <linux/list.h>
#include <linux/version.h>

#include "pts.h"
#include "find_kallsyms_lookup_name.h"

#define M(RET, NAME, SIGNATURE) \
    typedef RET (*NAME ## _t) SIGNATURE; \
    NAME ##_t g_ ##NAME
#include "IMPORT.M"
#undef M

#define MAX_ADDRESS 0xFFFF880000000000UL // Max user-space addresses for the x86 architecture

int pts_init(void)
{
    pr_debug("Initializing\n");

    #define M(RET, NAME, SIGNATURE) \
        if (!(g_ ## NAME = (NAME ##_t)\
                the_kallsyms_lookup_name(#NAME))) { \
            pr_err("Can't lookup '" #NAME "' function."); \
            return -1; \
        }
        #include "IMPORT.M"
    #undef M

    return 0;
}

void pts_cleanup(void)
{
    pr_debug("Cleaning up\n");
}


// == PIDs == {
#define PIDs_MAX 5
static DEFINE_MUTEX(PIDs_mtx);
static struct pid * PIDs[PIDs_MAX];
size_t PIDs_size = 0;

void PIDs_refresh(void)
// NB! should be called under PIDs_mtx lock
{
    size_t i;
    for (i = 0; i < PIDs_size; ++i) {
        struct task_struct * t = get_pid_task(PIDs[i], PIDTYPE_PID);
        if (t) {
            put_task_struct(t);
            continue;
        }
        pr_info("Process %d has gone.\n", pid_nr(PIDs[i]));
        put_pid(PIDs[i]);
        PIDs[i] = PIDs[--PIDs_size];
    }
}

int PIDs_bind(const pid_t nr)
{
    struct pid * p = NULL;
    struct mutex * m = NULL;
    size_t i;
    int rc = 0;

    p = find_get_pid(nr);
    if (!p) {
        pr_warn("Invalid pid value (%d): can't find pid.\n", nr);
        rc = -1; goto release_return;
    }

    mutex_lock(&PIDs_mtx); m = &PIDs_mtx;

    if (PIDs_size == ARRAY_SIZE(PIDs)) {
        pr_warn("Managed PIDs at capacity.\n");
        rc = -1; goto release_return;
    }

    for (i = 0; i < PIDs_size; ++i) {
        if (PIDs[i] == p) {
            pr_warn("Already managing given PID.\n");
            rc = -1; goto release_return;
        }
    }

    PIDs[PIDs_size++] = p; p = NULL;
    pr_info("Bound pid=%d.\n", nr);

release_return:
    if (m) mutex_unlock(m);
    if (p) put_pid(p);
    return rc;
}

int PIDs_unbind(const pid_t nr)
{
    struct pid * p = NULL;
    struct mutex * m = NULL;

    size_t i;
    int rc = 0;

    mutex_lock(&PIDs_mtx); m = &PIDs_mtx;

    for (i = 0; i < PIDs_size; ++i) {
        if (pid_nr(PIDs[i]) == nr) {
            p = PIDs[i];
            if (PIDs_size > 0) {
                PIDs[i] = PIDs[--PIDs_size];
            }
            pr_info("Unbound pid=%d.\n", nr);
            goto release_return;
        }
    }

release_return:
    if (m) mutex_unlock(m);
    if (p) put_pid(p);
    return rc;
}

// } == PIDs ==

// == PTS == {
  #define PTS_PFN_SHIFT 12 /*4Kb pages*/
  #define PTS_cache_HASH_BITS 10
  #define PTS_cache_SIZE 4*1024*1024 /*16 GiB*/
  static DEFINE_HASHTABLE(PTS_cache, PTS_cache_HASH_BITS);
  static DEFINE_MUTEX(PTS_mtx);
  static LIST_HEAD(PTS_hot_pages);
  //struct list_head head;

  struct PTS_PageInfo {
      struct hlist_node link;
      struct list_head  list;

      uint64_t local_clock;
      uint16_t accesses;

      uint64_t pfn : 52;

      bool is_hot : 1;
      bool is_slow : 1;

      //PTS_PageInfo(const uint64_t time = 0)
      //  : local_clock(time)
      //  , accesses(0)
      //  , hash_list(0)
      //  , is_hot(false)
      //  , is_slow(true)
      //{ }
  };

  uint64_t PTS_global_clock = 0;
  uint64_t PTS_cfg_cooling_threshold = 4;
  uint64_t PTS_cfg_hot_access_threshold = 4;
  uint64_t PTS_slow_hot_num = 0;

  struct PTS_PageInfo PTS_pages[PTS_cache_SIZE];
  size_t PTS_pages_idx = 0;

  struct PTS_PageInfo * PTS_PageInfo_create(uint64_t pfn)
  {
      struct PTS_PageInfo * pinfo;
      if (ARRAY_SIZE(PTS_pages) == PTS_pages_idx) {
          pr_warn("Run out of PageInfo elements (%lu)", PTS_pages_idx);
          return NULL;
      }

      pinfo = &PTS_pages[PTS_pages_idx++];
      //INIT_HLIST_NODE(&pinfo->link);
      pinfo->pfn = pfn;
      pinfo->local_clock = PTS_global_clock;
      pinfo->accesses = 0;
      pinfo->is_hot = false;
      pinfo->is_slow = true;

      hash_add(PTS_cache, &pinfo->link, pfn);

      return pinfo;
  }

  struct PTS_PageInfo * PTS_PageInfo_get(const int64_t pfn)
  {
      struct PTS_PageInfo * pinfo = NULL;
      hash_for_each_possible(PTS_cache, pinfo, link, pfn) {
          if (pfn == pinfo->pfn) return pinfo;
      }

      return PTS_PageInfo_create(pfn);
  }

// } == PTS ==




typedef int (*pte_entry_t)(pte_t *pte, unsigned long addr, unsigned long next, struct mm_walk *walk);

static int perform_pte_walk(pte_entry_t pte_callback, void * private)
{
    struct task_struct * t = NULL;
    struct mutex * m = NULL;
    struct mm_walk_ops mem_walk_ops = {.pte_entry = pte_callback};
    int i, rc = 0;

    mutex_lock(&PIDs_mtx); m = &PIDs_mtx;
    for (i = 0; i < PIDs_size; i++) {
        t = get_pid_task(PIDs[i], PIDTYPE_PID);
        if (!t) {
            pr_warn("Can't resolve task (%d).\n", pid_nr(PIDs[i]));
            continue;
        }
        spin_lock(&t->mm->page_table_lock);
        g_walk_page_range(t->mm, 0, MAX_ADDRESS, &mem_walk_ops, private);
        spin_unlock(&t->mm->page_table_lock);
        put_task_struct(t);
    }

    if (m) mutex_unlock(m);
    return rc;
}

// --------------------------------------------------------------------------------

static int pte_callback_clear(
        pte_t *ptep,
        unsigned long addr,
        unsigned long next,
        struct mm_walk *walk)
{
    pte_t old_pte;
    // If  page is not present, write protected
    if ((ptep == NULL)
    || !pte_present(*ptep)
    || !pte_write(*ptep)) {
        return 0;
    }

    old_pte = ptep_modify_prot_start(walk->vma, addr, ptep);
    *ptep = pte_mkold(old_pte); // unset modified bit
    *ptep = pte_mkclean(old_pte); // unset dirty bit
    ptep_modify_prot_commit(walk->vma, addr, ptep, old_pte, *ptep);

    return 0;
}

static int clear_ptes(void)
{
    pr_debug("Cleaning PTEs");
    return perform_pte_walk(pte_callback_clear, NULL);
}

// --------------------------------------------------------------------------------

static bool page_is_hot(struct PTS_PageInfo * pinfo)
{ return pinfo->accesses >= PTS_cfg_hot_access_threshold; }

static void adjust_temperature(struct PTS_PageInfo * pinfo)
{
    uint64_t dt = PTS_global_clock - pinfo->local_clock;
    if (!dt) return;

    pinfo->accesses >>= dt;
    pinfo->local_clock = PTS_global_clock;
}

static int pte_callback_walk(
        pte_t *ptep,
        unsigned long addr,
        unsigned long next,
        struct mm_walk *walk)
{
    const uint64_t pfn = addr >> PTS_PFN_SHIFT;
    struct PTS_PageInfo * pinfo;
    //struct pte_callback_context_t ? ctx =
    //    (struct pte_callback_context_t *) walk->private;
    //pte_t old_pte;

    // If page is not present or write protected
    if ((ptep == NULL)
    || !pte_present(*ptep)
    || !pte_write(*ptep)
    ) return 0;

    // if page is not dirty and wasn't modified
    if (!pte_dirty(*ptep) && !pte_young(*ptep)) {
        return 0;
    }

    if ((pinfo = PTS_PageInfo_get(pfn))) {
        adjust_temperature(pinfo);
        if (++pinfo->accesses == PTS_cfg_cooling_threshold) {
            ++PTS_global_clock;
        }
        if (!pinfo->is_hot && page_is_hot(pinfo)) {
            pinfo->is_hot = true;
            list_add(&pinfo->list, &PTS_hot_pages);
            if (pinfo->is_slow) {
                ++PTS_slow_hot_num;
            }
        }
    }

    //old_pte = ptep_modify_prot_start(walk->vma, addr, ptep);
    //*ptep = pte_mkold(old_pte); // unset modified bit
    //*ptep = pte_mkclean(old_pte); // unset dirty bit
    //ptep_modify_prot_commit(walk->vma, addr, ptep, old_pte, *ptep);
    return 0;
}

static int walk_ptes(void)
{
    pr_debug("Walking PTEs");
    return perform_pte_walk(pte_callback_walk, NULL);
}

// -------------------------------------------------------------------------------

int pts_hot_pages(size_t sz, size_t * addrs)
{
    bool warned = false;
    int idx = 0;
    struct PTS_PageInfo * pinfo;
    list_for_each_entry(pinfo, &PTS_hot_pages, list) {
        adjust_temperature(pinfo);
        if (!page_is_hot(pinfo)) {
            if (pinfo->is_slow) {
                --PTS_slow_hot_num;
            }
            list_del(&pinfo->list);
            pinfo->is_hot = false;

        }
        else {
            if (idx == sz) {
                if (!warned) {
                    warned = true;
                    pr_warn("Can't fit all hot pages to the %lu vector", sz);
                }
                continue;
            }
            addrs[idx++] = (pinfo->pfn << PTS_PFN_SHIFT);
        }
    }
    return idx;
}
