#define pr_fmt(fmt) "pts_kmod.main: " fmt

#include <linux/compiler.h>
#include <linux/init.h>
#include <linux/jiffies.h>
#include <linux/kallsyms.h>
#include <linux/kernel.h>   /* Needed for KERN_INFO */
//#include <linux/kstrtox.h>
#include <linux/mm.h>
#include <linux/module.h>   /* Needed by all modules */
#include <linux/pid.h>
#include <linux/proc_fs.h>  /* Necessary because we use the proc fs */
#include <linux/seq_file.h> /* for seq_file */
#include <linux/slab.h>
#include <linux/timer.h>
#include <linux/workqueue.h>

#include "find_kallsyms_lookup_name.h"
#include "tsc.h"
#include "pts.h"

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Ilia Kuzmin");
MODULE_DESCRIPTION("PageTableScane routine");
MODULE_VERSION("0.10");

/**
 * This function is called for each "step" of a sequence
 *
 */
static int pts_kmod_show(struct seq_file *s, void * private)
{
    pr_debug("Show\n");
    return 0;
}

static ssize_t pts_kmod_proc_write(
        struct file * file,
        const char __user * buffer,
        size_t count,
        loff_t * ppos)
{
    char * buf = NULL;
    ssize_t rc = count;

//    if (count > LED_MAX_LENGTH)
//        count = LED_MAX_LENGTH;
//
    //pr_info("proc_write\n");
    buf = memdup_user_nul(buffer, count);
    if (IS_ERR(buf))
        return PTR_ERR(buf);

    /* work around \n when echo'ing into proc */
    if (buf[count - 1] == '\n')
        buf[count - 1] = '\0';

    ///* before we change anything we want to stop any running timers,
    // * otherwise calls such as on will have no persistent effect
    // */
    //del_timer_sync(&led_blink_timer);

    if (!strncmp(buf, "run ", 4)) {
        unsigned thd, delay;
        if (sscanf(buf, "run %d %d", &thd, &delay) > 0) {
            pr_info("binding.... (pid:%i, thd:%u, delay:%d)\n", current->pid, thd, delay);
        }
        else {
            pr_info("Can't parse arguments for '%s'", buf);
        }
    }
    if (!strcmp(buf, "stop")) {
        pr_info("stop...");
    }
    else {
        pr_info("Unknown cmd '%s'\n", buf);
        rc = -EINVAL;
    }
    kfree(buf);
    return rc;
}

static int pts_kmod_proc_open(struct inode * node, struct file *file)
{
    return single_open(file, pts_kmod_show, NULL);
};

static const struct proc_ops pts_kmod_proc_ops = {
    .proc_open    = pts_kmod_proc_open,
    .proc_read    = seq_read,
    .proc_lseek   = seq_lseek,
    .proc_write   = pts_kmod_proc_write,
    .proc_release = single_release,
};

// ---------------------------------------------------------------------------------

static bool g_work_queue_die = false;
static unsigned g_time_interval = 1000;

static void work_queue_routine(struct work_struct *dummy);
static DECLARE_DELAYED_WORK(g_task, work_queue_routine);
static void work_queue_routine(struct work_struct *dummy)
{
    pr_info("schedule_event....");
    if (!g_work_queue_die) {
		schedule_delayed_work(&g_task, msecs_to_jiffies(g_time_interval));
    }
}

int work_queue_init(void)
{
    pr_debug("Initializing work queue");
    //g_workqueue = create_workqueue(WORK_QUEUE_NAME);
    work_queue_routine(NULL);
    return 0;
}

void work_queue_cleanup(void)
{
    pr_debug("Deinitializing work queue");
    g_work_queue_die = true;
    cancel_delayed_work_sync(&g_task);
    //destroy_workqueue(g_workqueue);
}

// ---------------------------------------------------------------------------------

#define PROC_FILE "pts_kmod"
int init_module(void)
{
    struct proc_dir_entry * entry;
    int rc;

    pr_info("Initialization\n");

    tsc_init();

    if ((rc = find_kallsyms_lookup_name())) {
        pr_warn("Can't lookup 'kallsyms_lookup_name'");
        return rc;
    }

    if ((rc = pts_init())) {
        pr_warn("Can't init PTS");
        return rc;
    }

    //pr_info("walk_page_range address = 0x%lx\n", the_kallsyms_lookup_name("walk_page_range"));

    entry = proc_create(PROC_FILE, 0666, NULL, &pts_kmod_proc_ops);
    if (!entry) {
        pr_warn("proc initialization failed");
        return -ENOMEM;
    }

    return 0;
    // FIXME!!!

    if ((rc = work_queue_init())) {
        return rc;
    }

    return 0;
}

void cleanup_module(void)
{
    pr_info("release\n");
	// FIXME: work_queue_cleanup();
    pts_cleanup();

    remove_proc_entry(PROC_FILE, NULL);
}

