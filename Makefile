obj-m += pts_kmod.o
pts_kmod-objs := main.o find_kallsyms_lookup_name.o tsc.o pts.o

KBUILD_CFLAGS += -fno-omit-frame-pointer
export DEBUG = YES

.tags:
	@ctags -f $@ -R .

all:
	@make -C /lib/modules/$(shell uname -r)/build M=$(PWD) modules

clean:
	@make -C /lib/modules/$(shell uname -r)/build M=$(PWD) clean

help:
	@make -C /lib/modules/$(shell uname -r)/build help

tags:
	@make -C /lib/modules/$(shell uname -r)/build M=$(PWD) tags

cscope:
	@make -C /lib/modules/$(shell uname -r)/build M=$(PWD) cscope

gtags:
	@make -C /lib/modules/$(shell uname -r)/build M=$(PWD) gtags

%.d: %.c
	@clang -MD -MF $@ -I/usr/lib/modules/$(shell uname -r)/build/include/ $^
	
insmod: ./pts_kmod.ko
	@make -s
	@sudo insmod ./pts_kmod.ko

rmmod:
	@sudo rmmod pts_kmod

run:
	@make -s rmmod || true
	@make -s insmod
	@watch cat /proc/pts_kmod


MEMX_OPT ?= "--no-bind"
run.memx:
	@make -s -C ../memx run NUMACTL='' MEMX='-w 40000 --limit_seconds 30 ${MEMX_OPT}' OMP_NUM_THREADS=8

run.memx.pts_kmod:
	@make rmmod || true
	@make insmod
	@make -s run.memx MEMX_OPT="--bind pts_kmod"
	@make rmmod || true

run.memx.ambix:
	@make -s -C ../ambix pcm ambixctl
	@make -s run.memx MEMX_OPT="--bind ambix"

.PHONY: run.memx.pts_kmod run.memx.ambix run.memx

status:
	@sh -c 'if ! pgrep status; then \
		tmux split-window -d "watch -n 1 -- ~/bin/status";   \
		tmux split-window -d "watch cat /proc/pts_kmod"; \
		fi'
	@#tmux split-window -d "watch -n 1 -- ~/bin/status"
	@#echo enable > /proc/pts_kmod
	@#tmux split-window -d "watch cat /proc/pts_kmod"


.PHONY: insmod rmmod status run
